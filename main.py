# Load model directly
from transformers import AutoTokenizer, AutoModelForMaskedLM
import os
from datasets import load_dataset

current_directory = os.path.dirname(__file__)
parent_directory = os.path.dirname(current_directory)
# grandparent_directory = os.path.dirname(parent_directory)
models_directory = os.path.join(parent_directory, 'base_models')

# tokenizer = AutoTokenizer.from_pretrained("HooshvareLab/bert-fa-zwnj-base")
# model = AutoModelForMaskedLM.from_pretrained("HooshvareLab/bert-fa-zwnj-base")

import torch
from transformers import BertForTokenClassification, BertTokenizer, DataCollatorForTokenClassification, Trainer, TrainingArguments
from datasets import load_dataset, load_metric

# Load the train and test datasets in CoNLL format
# Load your NER dataset
# dataset = load_dataset("conll2003", data_files={"train": parent_directory + "/datasets/ner_data/train.conll"})#, "validation": parent_directory +  "/datasets/ner_data/test.conll", "test": parent_directory + "/datasets/ner_data/test.conll"})

# train_dataset = load_dataset('conll2003', data_files={'train': parent_directory + '/datasets/train.conll'})
test_dataset = load_dataset(parent_directory + '/datasets/ner_data/test.conll')# data_files={'test': parent_directory + '/datasets/ner_data/test.conll'})
# print(train_dataset)11
# print(test_dataset)
# data = load_dataset("conll2003")
# data = load_dataset()
# num_labels=train_dataset['train'].features['ner_tags'].feature.num_classes
print(test_dataset)
#
# # Load the BERT tokenizer and model
# tokenizer = BertTokenizer.from_pretrained(models_directory + '/bert-base-parsbert-uncased')
# model = BertForTokenClassification.from_pretrained(models_directory + '/bert-base-parsbert-uncased', num_labels=dataset['train'].features['ner_tags'].feature.num_classes)
#
# # Define training arguments
# training_args = TrainingArguments(
#     output_dir="./ner_model",
#     evaluation_strategy="steps",
#     eval_steps=100,
#     per_device_train_batch_size=8,
#     per_device_eval_batch_size=8,
#     num_train_epochs=3,
#     save_total_limit=2,
#     load_best_model_at_end=True,
#     logging_dir="./logs",
#     logging_steps=10,
# )
#
# # Prepare data collator
# data_collator = DataCollatorForTokenClassification(tokenizer)
#
# # Create a Trainer
# trainer = Trainer(
#     model=model,
#     args=training_args,
#     data_collator=data_collator,
#     train_dataset=dataset['train'],
#     eval_dataset=dataset['test'],
# )
#
# # Fine-tune the model
# trainer.train()
#
# # Evaluate the model
# results = trainer.evaluate()
#
# # Save the model
# model.save_pretrained("./fine_tuned_ner_model")
# tokenizer.save_pretrained("./fine_tuned_ner_model")
#
# # Use the model for NER predictions
