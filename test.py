
from transformers import AutoTokenizer, AutoModelForMaskedLM, BertForTokenClassification

tokenizer = AutoTokenizer.from_pretrained("albert-base-v2")
model = AutoModelForMaskedLM.from_pretrained("albert-base-v2")
